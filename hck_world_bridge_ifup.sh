#!/bin/sh

# Copyright (c) 2014, Swortex A.A. Ltd (www.swortex.com)
# Copyright (c) 2013, Daynix Computing LTD (www.daynix.com)
# All rights reserved.
#
# Maintained by vmwhql@swortex.com
#
# This code is licensed under standard 3-clause BSD license.
# See file LICENSE supplied with this package for the full license text.

. `dirname $0`/vmwhql_setup.cfg
. `dirname $0`/vmwhql_env.sh

enslave_iface ${WORLD_BR_NAME} $1
