<#
.SYNOPSIS
    Sets up a vmWHQL client VM
.DESCRIPTION
    The scripts takes care of the VM name, firewall and messaging adapter name.
.PARAMETER setupType
    vmWHQL setup type (HCK, SVVP etc.)
.PARAMETER setupOSVer
    vmWHQL setup OS Version (W2008, W2008R2, W2012, W2012R, W2016 etc.)
.EXAMPLE
    C:\PS> vmwhql_setup_client_vm.ps1 -setupType HCK -setupOSVer W2008R2
	This command configures a client that's a part of vmWHQL HCK setup for Windows 2008 R2.
.NOTES
    Authors: Anton Nayshtut, Max Turchin, Dima Novikov
    Date:   June 2, 2015
#>

[CmdletBinding()]
Param(
  [Parameter(Mandatory=$True,
             HelpMessage='vmWHQL setup type (HCK, SVVP etc.)')]
   [ValidateLength(3,4)]
   [string]$setupType
)

$msgNetMAC = '56:CC:CC:0*:CC:CC'
$wldNetMAC = '22:11:11:11:0*'
$hckPort = '1771'
$msgNetIPSub = '192.168.100.'
$legacyHxKStudioName = "HCK-Studio"

$msgNetAdapter = $null
$msgNetConfig  = $null
$wldNetAdapter = $null
$wldNetConfig  = $null
$clientNo      = $null

function GetNetCfg
{
Param(
	[Parameter(Mandatory=$True)]
		$objNet
)
	$objNetCfg = Get-WmiObject Win32_NetworkAdapterConfiguration -Filter "Index = '$($objNet.Index)'"
	if (!$objNetCfg) {
		throw "ERROR: Cannot get configuration for Network Adapter: " + $objNet.NetConnectionID
	}

	return $objNetCfg
}

function GetClientNo
{
	$clientNoChar=$msgNetAdapter.MacAddress.ToCharArray()[10]
	return [convert]::ToInt32($clientNoChar, 10)
}

function RenameComputer
{
Param(
	[Parameter(Mandatory=$True)]
		[ValidateLength(5,15)]
		[string]$newName
)
	$computer = Get-WmiObject -Class Win32_ComputerSystem
	if (!$computer) {
		throw "ERROR: Cannot get the computer object"
	}
	if (($computer.Name -ne $newName) -and ($computer.Name -ne $legacyHxKStudioName)) {
		Write-Host "Renaming VM#" $clientNo ":" $computer.Name "to" $newName
		$result = $computer.Rename($newName)
		if ($result.ReturnValue -ne 0) {
			throw "ERROR: Cannot rename VM#" + $clientNo
		}
	}
}

function GetIPv4Addr
{
Param(
	[Parameter(Mandatory=$True)]
		$objNetCfg
)
	return $objNetCfg.IPAddress[0]
}

function BcdeditDo
{
	& bcdedit $args
	if ($LastExitCode) {
		throw "ERROR: Cannot execute bcdedit " + $args
	}
}

function NetshFwlDo
{
	& netsh advfirewall $args
	if ($LastExitCode) {
		throw "ERROR: Cannot execute netsh advfirewall " + $args
	}
}

function SetupFirewall
{
	NetshFwlDo reset
	if ($LastExitCode) {
		throw "ERROR: Cannot reset firewall"
	}

	if ($msgNetAdapter) {
		$IPv4Address = $msgNetIPSub + $clientNo

		Write-Host "Setting up Message Network Adapter (" $msgNetAdapter.Name ":" $IPv4Address ")"

		NetshFwlDo firewall add rule name = `"vmWHQL Message Device Allow All In`" dir = in localip = `"$IPv4Address`" action = allow
		NetshFwlDo firewall add rule name = `"vmWHQL Message Device Allow All Out`" dir = out remoteip = `"$IPv4Address/24`" action = allow
	}

	if ($wldNetAdapter) {
		$IPv4Address = GetIPv4Addr $wldNetConfig
		Write-Host "Setting up World Network Adapter (" $wldNetAdapter.Name ":" $IPv4Address ")"
		NetshFwlDo firewall add rule name = `"vmWHQL World Device Block HCK In`" dir = in localip = `"$IPv4Address`" protocol = tcp localport = $hckPort action = block
		NetshFwlDo firewall add rule name = `"vmWHQL World Device Block HCK Out`" dir = out remoteip = `"$IPv4Address/24`" protocol = tcp localport = $hckPort action = block
	}
}

function EditVMxRegistry
{
    $AutoUpdatePath = "registry::HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\WindowsUpdate\Auto Update"
    $ScreenSavePath = "registry::HKEY_CURRENT_USER\Control Panel\Desktop"
    $ScreenLockPath = "registry::HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\Personalization"

    Set-ItemProperty -Path "$AutoUpdatePath" -Name AUOptions -Value 1
    Set-ItemProperty -Path "$ScreenSavePath" -Name ScreenSaveActive -Value 0
	$PathExist = Test-Path "$ScreenLockPath"
    if(!$PathExist) {
		New-Item -Path "$ScreenLockPath"
	}
    Set-ItemProperty -Path "$ScreenLockPath" -Name NoLockScreen -Value 1
}

function ConfigHxKVM
{
#	Write-Host "Renaming Message Device (" $msgNetAdapter.NetConnectionID ") on VM#" $clientNo
#	$msgNetAdapter.NetConnectionID = "MessageDevice"
#	$msgNetAdapter.Put()

	Write-Host "Disabling LUA for VM#" $clientNo
	Set-ItemProperty -Path HKLM:\Software\Microsoft\Windows\CurrentVersion\policies\system -Name EnableLUA -Value 0 -Force

	Write-Host "Configuring IP for Messaging Adapter on VM#" $clientNo
	$msgNetConfig.EnableStatic($msgNetIPSub + $clientNo, "255.255.255.0");
	$msgNetConfig.SetDNSServerSearchOrder();
	if ($clientNo -ne 1) {
		$msgNetConfig.SetGateways($msgNetIPSub + "1", 1);
	}

	SetupFirewall

	winrm set winrm/config/client/auth '@{Basic="true"}'
	winrm set winrm/config/service/auth '@{Basic="true"}'
	winrm set winrm/config/service '@{AllowUnencrypted="true"}'
}

function ConfigHxKStudioVM
{
	ConfigHxKVM
	RenameComputer $($setupType + "-STUDIO")
}

function ConfigHxKClientVM
{
	ConfigHxKVM
	RenameComputer $($setupType + "-CLIENT" + ($clientNo - 1))
	Write-Host "Switching TESTSIGNING ON on VM#" $clientNo
	BcdeditDo -set TESTSIGNING ON
	BcdeditDo -set LOADOPTIONS DISABLE_INTEGRITY_CHECKS
	$OSRecovery_config = Get-WmiObject Win32_OSRecoveryConfiguration -EnableAllPrivileges
	$OSRecovery_config | Set-WmiInstance -Arguments @{AutoReboot=$True ; DebugInfoType=2}
}

Write-Host "Searching for messaging device..."
$msgNetAdapter = Get-WmiObject -Class Win32_NetworkAdapter | Where-Object {$_.MACAddress -like $msgNetMAC}
if (!$msgNetAdapter) {
	throw "ERROR: Cannot find Messaging Network Adapter"
}

$msgNetConfig = GetNetCfg $msgNetAdapter

Write-Host "Searching for world device..."
$wldNetAdapter = Get-WmiObject -Class Win32_NetworkAdapter | Where-Object {$_.MACAddress -like $wldNetMAC}
if (!$wldNetAdapter) {
	Write-Host "WARNING: Cannot find World Network Adapter"
}
else {
	$wldNetConfig = GetNetCfg $wldNetAdapter
}

EditVMxRegistry
$clientNo=GetClientNo

if ($clientNo -eq 1) {
	ConfigHxKStudioVM
}
else {
	ConfigHxKClientVM
}

Write-Host "Done!"

Restart-Computer


