::
:: Copyright (c) 2014, Swortex A.A. Ltd (www.swortex.com)
:: Copyright (c) 2013, Daynix Computing LTD (www.daynix.com)
:: All rights reserved.
::
:: Maintained by vmwhql@swortex.com
::
:: This code is licensed under standard 3-clause BSD license.
:: See file LICENSE supplied with this package for the full license text.
::
bcdedit /set groupsize 2
IF ERRORLEVEL 1 goto error
bcdedit /set groupaware on
IF ERRORLEVEL 1 goto error
shutdown -r -t 0
REM We sholdn't be here if no errors occured
:error
ECHO "ERROR: CPU grouping setup has failed!"
