::
:: Copyright (c) 2014, Swortex A.A. Ltd (www.swortex.com)
:: Copyright (c) 2013, Daynix Computing LTD (www.daynix.com)
:: All rights reserved.
::
:: Maintained by vmwhql@swortex.com
::
:: This code is licensed under standard 3-clause BSD license.
:: See file LICENSE supplied with this package for the full license text.
::
echo Disable external network...
wmic path win32_networkadapter where (macaddress like "22:11:11:11:0%%") call disable

echo Run HCK studio...
cd "%WTTSTDIO%"
"%WTTSTDIO%\hckstudio.exe"

echo Enable network...
wmic path win32_networkadapter where (name like "Intel%%") call enable
