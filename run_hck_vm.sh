#!/bin/sh
#
# Copyright (c) 2014, Swortex A.A. Ltd (www.swortex.com)
# Copyright (c) 2013, Daynix Computing LTD (www.daynix.com)
# All rights reserved.
#
# Maintained by vmwhql@swortex.com
#
# This code is licensed under standard 3-clause BSD license.
# See file LICENSE supplied with this package for the full license text.

VMWHQL_DIR=`dirname $0`

. $VMWHQL_DIR/vmwhql_setup.cfg
. $VMWHQL_DIR/vmwhql_env.sh

VM_NO=$1

VM_PORT=$(vm_video_port_no $VM_NO)
VM_IMAGE=$(vm_var_val $VM_NO IMAGE)
VM_ROLE=$(vm_var_val $VM_NO ROLE)
VM_CPUS=$(vm_var_val $VM_NO CPUS)
VM_MEMORY=$(vm_var_val $VM_NO MEMORY)
VM_EXTRA=$(vm_var_val $VM_NO EXTRA)
VM_CPU_MODEL=$(vm_var_val $VM_NO CPU_MODEL)
VM_NOQ=$(vm_var_val $VM_NO NOQ)
VM_LUNS=$(vm_var_val $VM_NO LUNS)
VM_CONSOLE_TO_SOCKET=$(vm_var_val $VM_NO CONSOLE_TO_SOCKET)

echo "Starting HCK VM ${VM_NO} (${VM_ROLE})"

vm_ctrl_ifname()
{
  echo hck_ctl_${UNIQUE_ID}_${VM_NO}
}

vm_test_ifname()
{
  DEVICE_NUM=$1

  echo hck_tst_${UNIQUE_ID}_${VM_NO}_${DEVICE_NUM}
}

vm_ctrl_mac()
{
  echo 56:cc:cc:0${VM_NO}:cc:cc
}

vm_test_mac()
{
  DEVICE_NUM=$1

  echo 56:cc:cc:0${VM_NO}:0${DEVICE_NUM}:cc
}

TEST_IMAGE_NAME_BASE=$(dirname ${VM_IMAGE})/client${VM_NO}_test_image

prepare_test_image()
{
  test -f ${TEST_IMAGE_NAME_BASE}$1.raw || \
  { echo Creating test image ${TEST_IMAGE_NAME_BASE}$1.raw...; qemu-img create -f raw ${TEST_IMAGE_NAME_BASE}$1.raw 30G; }
}


check_global_cfg
if [ $? -ne 0 ]; then
        exit 1
fi
WORLD_NET_IFACE="-netdev tap,id=hostnet9,script=${HCK_ROOT}/hck_world_bridge_ifup.sh,downscript=no,ifname=hck_wld_${UNIQUE_ID}_${VM_NO}
                  -device ${WORLD_NET_DEVICE},netdev=hostnet9,mac=22:11:11:11:0${VM_NO}:${UNIQUE_ID},bus=pci.0,id=hck_wld_${UNIQUE_ID}_${VM_NO}"
CTRL_NET_IFACE="-netdev tap,id=hostnet0,script=${HCK_ROOT}/hck_ctrl_bridge_ifup.sh,downscript=no,ifname=`vm_ctrl_ifname`
                 -device ${CTRL_NET_DEVICE},netdev=hostnet0,mac=`vm_ctrl_mac`,bus=pci.0,id=`vm_ctrl_ifname`"

IDE_BOOT_STORAGE_PAIR="-drive file=${VM_IMAGE},if=ide,serial=${VM_NO}110${UNIQUE_ID}${DRIVE_CACHE_OPTION}"
BLK_BOOT_STORAGE_PAIR="-drive file=${VM_IMAGE},if=none,id=vio_block,serial=${VM_NO}110${UNIQUE_ID}${DRIVE_CACHE_OPTION}
                           -device virtio-blk-pci,bus=pci.0,drive=vio_block"
SCSI_BOOT_STORAGE_PAIR="-drive file=${VM_IMAGE},if=none,id=bootscsi,serial=${VM_NO}110${UNIQUE_ID}${DRIVE_CACHE_OPTION}
                            -device virtio-scsi-pci,bus=pci.0
                            -device scsi-hd,drive=bootscsi"

VM_GRAPHICS=

case $VIDEO_TYPE in
VNC)
    VM_GRAPHICS="-vnc :${VM_PORT}"
    ;;
SPICE)
    VM_GRAPHICS="-spice port=${VM_PORT},disable-ticketing -vga qxl"
    ;;
*)
        echo "ERROR: Unknown Video type: $VIDEO_TYPE"
        ;;
esac

GUEST_WINRM_PORT_NO=5985
if test x"${VM_REMOTING}" = xon; then
    mkdir -p -m 777 -- "/tmp/vmwhql_smb/${UNIQUE_ID}"
    REMOTING_OPTION="-netdev user,id=mynet0,smb=/tmp/vmwhql_smb/${UNIQUE_ID},hostfwd=tcp:127.0.0.1:$(vm_host_winrm_port_no $VM_NO)-:${GUEST_WINRM_PORT_NO} -device e1000,netdev=mynet0"
fi
test x"${VM_CONSOLE_TO_SOCKET}" = xon && MONITOR_OPTION="-monitor unix:`dirname $0`/qemu_monitor_${UNIQUE_ID}_client${VM_NO}_socket,server,nowait"
test x"${SNAPSHOT}" = xon && SNAPSHOT_OPTION="-snapshot"
test x"${UNSAFE_CACHE}" = xon && DRIVE_CACHE_OPTION=",cache=unsafe"
test x"${VM_CPU_MODEL}" = x"" && VM_CPU_MODEL="qemu64,+x2apic"
test x"${MOUNT_GUEST_TOOLS}" = xon && COMMON_EXTRA_OPTS="-drive file=fat:${VMWHQL_DIR}/guest_tools/"

case $VM_ROLE in
hck_client)
    BOOT_STORAGE_PAIR=$IDE_BOOT_STORAGE_PAIR
    test x"${FORCE_WORLD_NET_IFACE}" != xon && WORLD_NET_IFACE=
    case $HCK_TEST_DEV_TYPE in
    network)
        test x"${VIRTIO_BLK_BOOT}" = xon && BOOT_STORAGE_PAIR=$BLK_BOOT_STORAGE_PAIR
        if test x"$VM_NOQ" != x""  &&  test x"$VM_NOQ" != x"1"; then
                VM_NOV=$((${VM_NOQ} * 2 + 2))
                VM_MQ_NETDEV_PARAM=",queues=${VM_NOQ}"
                VM_MQ_DEVICE_PARAM=",mq=on,vectors=${VM_NOV}"
        fi
        TEST_NET_DEVICES="-netdev tap,id=hostnet2,vhost=${VHOST_STATE},script=${HCK_ROOT}/hck_test_bridge_ifup.sh,downscript=no,ifname=`vm_test_ifname 1`${VM_MQ_NETDEV_PARAM}
                     -device virtio-net-pci,netdev=hostnet2,mac=`vm_test_mac 1`,bus=pci.0${VM_MQ_DEVICE_PARAM},id=`vm_test_ifname 1`"
        ;;
    bootstorage)
        BOOT_STORAGE_PAIR=$BLK_BOOT_STORAGE_PAIR
        ;;
    storage)
        BOOT_STORAGE_PAIR=$IDE_BOOT_STORAGE_PAIR
        TEST_STORAGE_PAIR="-drive file=${TEST_IMAGE_NAME_BASE}.raw,if=none,id=virtio_blk,serial=${VM_NO}000${UNIQUE_ID}${DRIVE_CACHE_OPTION}
                      -device virtio-blk-pci,bus=pci.0,drive=virtio_blk"
        prepare_test_image ""
        ;;
    bootscsi)
        BOOT_STORAGE_PAIR=$SCSI_BOOT_STORAGE_PAIR
        ;;
    scsi)
        if test x"$VM_NOQ" != x""  &&  test x"$VM_NOQ" != x"1"; then
                VM_MQ_SCSI_PARAM=",num_queues=${VM_NOQ}"
        fi
        BOOT_STORAGE_PAIR=$IDE_BOOT_STORAGE_PAIR
        TEST_STORAGE_PAIR="-device virtio-scsi-pci${VM_MQ_SCSI_PARAM},bus=pci.0"
        LUN_SERIAL_NUM=0
        if test x"$VM_LUNS" = x""; then
                VM_LUNS="1"
        fi
        while [ $LUN_SERIAL_NUM -lt $VM_LUNS ]
        do
           TEST_STORAGE_PAIR=$TEST_STORAGE_PAIR" -drive file=${TEST_IMAGE_NAME_BASE}$LUN_SERIAL_NUM.raw,if=none,id=scsi${LUN_SERIAL_NUM},serial=${VM_NO}000${UNIQUE_ID}${DRIVE_CACHE_OPTION}
                         -device scsi-hd,drive=scsi${LUN_SERIAL_NUM},scsi-id=0,bus=scsi.0,lun=${LUN_SERIAL_NUM}"
           prepare_test_image $LUN_SERIAL_NUM
           LUN_SERIAL_NUM=`expr $LUN_SERIAL_NUM + 1`
        done
        ;;
    serial)
        test x"${VIRTIO_BLK_BOOT}" = xon && BOOT_STORAGE_PAIR=$BLK_BOOT_STORAGE_PAIR
        TEST_SERIAL_DEVICES="-device virtio-serial-pci,bus=pci.0"
        ;;
    balloon)
        test x"${VIRTIO_BLK_BOOT}" = xon && BOOT_STORAGE_PAIR=$BLK_BOOT_STORAGE_PAIR
        TEST_BALLOON_DEVICE="-device virtio-balloon-pci,bus=pci.0"
        ;;
    rng)
        test x"${VIRTIO_BLK_BOOT}" = xon && BOOT_STORAGE_PAIR=$BLK_BOOT_STORAGE_PAIR
        TEST_RNG_DEVICE="-device virtio-rng-pci,bus=pci.0"
        ;;
    *)
        echo "ERROR: Unknown HCK Test type: $HCK_TEST_DEV_TYPE"
        ;;
    esac
    ;;
hck_studio | svvp_studio)
    BOOT_STORAGE_PAIR="-drive file=${VM_IMAGE},if=ide${DRIVE_CACHE_OPTION}"
    ;;
svvp_dc)
    test x"${FORCE_WORLD_NET_IFACE}" != xon && WORLD_NET_IFACE=
    BOOT_STORAGE_PAIR="-drive file=${VM_IMAGE},if=ide${DRIVE_CACHE_OPTION}"
    ;;
svvp_mc | svvp_sc)
    BOOT_STORAGE_PAIR="-drive file=${VM_IMAGE},if=ide${DRIVE_CACHE_OPTION}"
    test x"${FORCE_WORLD_NET_IFACE}" != xon && WORLD_NET_IFACE=
    CTRL_NET_IFACE=
    # virtio-net-pci is used as the only network device.
    TEST_NET_DEVICES="-netdev tap,id=hostnet2,vhost=${VHOST_STATE},script=${HCK_ROOT}/hck_ctrl_bridge_ifup.sh,downscript=no,ifname=`vm_test_ifname 1`
                     -device ${SVVP_CL_NET_DEVICE},netdev=hostnet2,mac=`vm_test_mac 1`,bus=pci.0,id=`vm_test_ifname 1`"
    ;;
svvp_sut)
    BOOT_STORAGE_PAIR=$BLK_BOOT_STORAGE_PAIR
    test x"${FORCE_WORLD_NET_IFACE}" != xon && WORLD_NET_IFACE=
    CTRL_NET_IFACE=
    # virtio-net-pci is used as the only network device.
    TEST_NET_DEVICES="-netdev tap,id=hostnet2,vhost=${VHOST_STATE},script=${HCK_ROOT}/hck_ctrl_bridge_ifup.sh,downscript=no,ifname=`vm_test_ifname 1`
                     -device virtio-net-pci,netdev=hostnet2,mac=`vm_test_mac 1`,bus=pci.0,id=`vm_test_ifname 1`"
    TEST_SERIAL_DEVICES="-device virtio-serial-pci,bus=pci.0"
    TEST_BALLOON_DEVICE="-device virtio-balloon-pci,bus=pci.0"
    TEST_RNG_DEVICE="-device virtio-rng-pci,bus=pci.0"
    ;;
svvp_sut_install)
    BOOT_STORAGE_PAIR=$IDE_BOOT_STORAGE_PAIR
    TEST_STORAGE_PAIR="-drive file=${TEST_IMAGE_NAME_BASE}.raw,if=none,id=virtio_blk,serial=${VM_NO}000${UNIQUE_ID}${DRIVE_CACHE_OPTION}
                      -device virtio-blk-pci,bus=pci.0,drive=virtio_blk"
    prepare_test_image ""
    TEST_NET_DEVICES="-netdev tap,id=hostnet2,vhost=${VHOST_STATE},script=${HCK_ROOT}/hck_ctrl_bridge_ifup.sh,downscript=no,ifname=`vm_test_ifname 1`
                     -device virtio-net-pci,netdev=hostnet2,mac=`vm_test_mac 1`,bus=pci.0,id=`vm_test_ifname 1`"
    TEST_SERIAL_DEVICES="-device virtio-serial-pci,bus=pci.0"
    TEST_BALLOON_DEVICE="-device virtio-balloon-pci,bus=pci.0"
    TEST_RNG_DEVICE="-device virtio-rng-pci,bus=pci.0"
    ;;
*)
    echo "ERROR: Unknown role: $VM_ROLE"
    ;;
esac

test x"${SUDO_USER}" != x"" && SETUP_OWNER=$SUDO_USER

${QEMU_BIN} \
        ${BOOT_STORAGE_PAIR} \
        ${TEST_STORAGE_PAIR} \
        ${CTRL_NET_IFACE} \
        ${TEST_NET_DEVICES} \
        ${TEST_SERIAL_DEVICES} \
        ${TEST_BALLOON_DEVICE} \
        ${TEST_RNG_DEVICE} \
        ${WORLD_NET_IFACE} \
        ${COMMON_EXTRA_OPTS} \
        -m ${VM_MEMORY} -smp ${VM_CPUS},cores=${VM_CPUS} -enable-kvm -cpu ${VM_CPU_MODEL} \
        -usbdevice tablet -boot d \
        -uuid CDEF127c-8795-4e67-95da-8dd0a889100${VM_NO} \
        -name HCK-${VM_ROLE}_${VM_NO}_${UNIQUE_ID}_${SETUP_OWNER}_`hostname` \
        ${REMOTING_OPTION} \
        ${MONITOR_OPTION} \
        ${VM_GRAPHICS} ${SNAPSHOT_OPTION} ${VM_EXTRA}
