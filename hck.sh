#!/bin/sh

# Copyright (c) 2014, Swortex A.A. Ltd (www.swortex.com) 
# Copyright (c) 2013, Daynix Computing LTD (www.daynix.com)
# All rights reserved.
#
# Maintained by vmwhql@swortex.com
#
# This code is licensed under standard 3-clause BSD license.
# See file LICENSE supplied with this package for the full license text.

VM_START_TIMEOUT=10

VMS_TO_RUN=

SCRIPTS_DIR=$(dirname $0)

if [ ! -f "$SCRIPTS_DIR/vmwhql_setup.cfg" ];
then
  echo "$SCRIPTS_DIR/vmwhql_setup.cfg does not exist. Create vmwhql_setup.cfg configuration file according to $SCRIPTS_DIR/vmwhql_setup.cfg.hck_example and $SCRIPTS_DIR/vmwhql_setup.cfg.svvp_example configuration file examples."
  exit 1
fi

. ${SCRIPTS_DIR}/vmwhql_setup.cfg
. ${SCRIPTS_DIR}/vmwhql_env.sh

if test x"$NOF_VMS" = x""
then
  echo NOF_VMS has to be defined within ${SCRIPTS_DIR}/vmwhql_setup.cfg
  exit 1
fi

usage()
{
    echo "vmWHQL launcher script"
    echo ""
    echo "sudo $0"
    echo "\t-h --help              - show this help"
    echo "\t-r=<no> --run-vms=<no> - run HCK Studio VM (default: $(seq -s ' ' 1 $NOF_VMS))"
    echo ""
}

PARAMS_ERROR=0

while [ "$1" != "" ]; do
    PARAM=`echo $1 | awk -F= '{print $1}'`
    VALUE=`echo $1 | awk -F= '{print $2}'`
    case $PARAM in
        -h | --help)
            usage
            exit
            ;;
        -r | --run-vms)
            test x"$VALUE" = x"" && PARAMS_ERROR=1
            [ $VALUE -le 0 ] && PARAMS_ERROR=1
            [ $VALUE -gt $NOF_VMS ] && PARAMS_ERROR=1
            VMS_TO_RUN="$VMS_TO_RUN $VALUE"
            ;;
         *)
            echo "ERROR: unknown parameter \"$PARAM\""
            usage
            exit 1
            ;;
    esac
    shift
done

if [ $PARAMS_ERROR -ne 0 ]; then
  echo "ERROR: wrong parameters"
  usage
  exit 1
fi

if test x`whoami` != xroot
then
  echo This script must be run as superuser
  exit 1
fi

kill_jobs() {
  jobs -p > /tmp/.jobs_$$
  kill `cat /tmp/.jobs_$$ | tr '\n' ' '`
  rm -f /tmp/.jobs_$$
}

check_global_cfg
if [ $? -ne 0 ]; then
    exit 1
fi

echo
dump_config
echo

trap "kill_jobs; loop_run_reset; remove_bridges; exit 0" INT

echo Creating bridges...
create_bridges

loop_run_reset
if test x"$VMS_TO_RUN" = x""; then
  VMS_TO_RUN=$(seq 1 $NOF_VMS)
fi

for i in $VMS_TO_RUN
do
  loop_run_vm ${SCRIPTS_DIR}/run_hck_vm.sh $i &
  sleep $VM_START_TIMEOUT
done

read -p "Press ENTER to disable VMs respawn..." NOT_NEEDED_VAR
loop_run_stop
echo VMs won\'t respawn anymore.
wait

sleep 2
remove_bridges
loop_run_reset

