import argparse
import os
import os.path
from winrm.protocol import Protocol
#from ConfigParser import ConfigParser
#from StringIO import StringIO


class Setup:
    def __init__(self, setup_id, vm_num):
        # If MAX_VMs_PER_SETUP, REMOTE_PORT_BASE or winrm_port_num_on_host formula changes appropriate changes must be made in vmwhql_env.sh as well
        max_vms_per_setup = 5
        remote_port_base = 50000
        winrm_port_num_on_host = int(setup_id) * int(max_vms_per_setup) + int(remote_port_base) + int(vm_num) - 1
        self.endpoint = 'http://127.0.0.1:' + str(winrm_port_num_on_host) + '/wsman'
        self.username = os.environ["GUEST_USERNAME"]
        self.password = os.environ["GUEST_PASSWORD"]
        self.server_cert_validation = 'ignore'


class Vm:
    def __init__(self, setup):
        self.attach(setup)

    def attach(self, setup):
        self.p = Protocol(endpoint=setup.endpoint,
                          username=setup.username,
                          password=setup.password,
                          server_cert_validation=setup.server_cert_validation)

    def execute_command(self, command_2_run):
        self.shell_id = self.p.open_shell()
        self.command_id = self.p.run_command(self.shell_id, command_2_run)
        std_out, std_err, status_code = self.p.get_command_output(self.shell_id, self.command_id)
        print ("err %s" % std_err)
        print ("out %s" % std_out)

    def dettach(self):
        self.p.cleanup_command(self.shell_id, self.command_id)
        self.p.close_shell(self.shell_id)


def get_args():
    # This function parses and return arguments passed in
    # Assign description to the help doc
    parser = argparse.ArgumentParser(description='Run command on guest')

    # Add arguments
    parser.add_argument('-s', '--setup_id', help='Setup ID', required=True)
    parser.add_argument('-v', '--vm_no', help='VM number', required=True)
    parser.add_argument('-c', '--command_to_run', help='Command to run', required=True)
    parser.add_argument('-u', '--guest_username', help='Guest username')
    parser.add_argument('-p', '--guest_password', help='Guest password')

    # Array for all arguments passed to script
    args = parser.parse_args()

    # Assign args to variables
    setup_id = args.setup_id
    vm_no = args.vm_no
    command_to_run = args.command_to_run
    guest_username = args.guest_username
    guest_password = args.guest_password

    # Return all variable values
    return setup_id, vm_no, command_to_run, guest_username, guest_password

# Match return values from get_arguments()
# and assign to their respective variables
setup_id, vm_no, command_to_run, guest_username, guest_password = get_args()

cred_file = './vm_credentials.txt'

if (guest_username is None or guest_password is None) and (os.path.isfile(cred_file) is False):
    print ("Guest Username and Password must be provided")
    quit()

if (os.path.isfile(cred_file) is False) or (guest_username and guest_password):
    with open('vm_credentials.txt', 'w') as file:
        file.write(guest_username + ':' + guest_password + '\n')

with open('vm_credentials.txt') as file:
    guest_username, guest_password = file.read().strip().split(':')

os.environ['GUEST_USERNAME'] = guest_username
os.environ['GUEST_PASSWORD'] = guest_password

setup = Setup(setup_id, vm_no)
vm = Vm(setup)
vm.execute_command(command_to_run)
vm.dettach()
