#!/bin/sh

usage ()
{
    echo "$SCRIPT_NAME - Stress test"
    echo ""
    echo "Usage:"
    echo "  options:"
    echo "      -h"
    echo "      --help - show this help"
    echo "      -u <vm-number>"
    echo "      --user-name <user-name> - enter VM user name (necessary only if vm_credentials.txt doesn't exist)"
    echo "      -p <password>"
    echo "      --password <password> - enter VM password (necessary only if vm_credentials.txt doesn't exist)"
    echo "      -si <setup-id>"
    echo "      --setup-id <setup-id> - set desired setup ID"
    echo "      -v <vm-number>"
    echo "      --vm-number <vm-number> - set desired vm number"
    echo "      -l <add-lun>"
    echo "      --add-lun <add-lun> - set number of LUN to be added"
    echo "      -s <lun-size>"
    echo "      --lun-size <lun-size> - set LUN size in GB (default size is ${DEFAULT_LUN_SIZE}GB)"
    echo "      -c <command-to-execute>"
    echo "      --command-to-execute <command-to-execute> - set command-to-execute"
    echo "      -t <time-interval>"
    echo "      --time-interval <time-interval> - set time interval in seconds between operations"
    echo "      -nol <number-of-luns>"
    echo "      --number-of-luns <number-of-luns> - set total number of LUNs"
    echo "      -it <iterations>"
    echo "      --iterations <iterations> - set number of iterations for test"

    return 0
}

process_command_line ()
{
    if [ "$1" != "" ]
    then
      while [ "$1" != "" ]
      do
          case $1 in
              -h|--help)
                    return 1
                    ;;
              -u|--user-name)
                  shift
                  USER_NAME=$1
                  ;;
              -p|--password)
                  shift
                  PASSWORD=$1
                  ;;
              -si|--setup-id)
                  shift
                  SETUP_ID=$1
                  ;;
              -v|--vm-number)
                  shift
                  VM_NO=$1
                  ;;
              -l|--add-lun)
                  shift
                  ADD_LUN=$1
                  ;;
              -s|--lun-size)
                  shift
                  LUN_SIZE=$1
                  ;;
              -c|--command-to-execute)
                  shift
                  COMMAND_TO_EXECUTE=$1
                  ;;
              -t|--time-interval)
                  shift
                  TIME_INTERVAL=$1
                  ;;
              -nol|--number-of-luns)
                  shift
                  NOLUNS=$1
                  ;;
              -it|--iterations)
                  shift
                  ITERATIONS=$1
                  ;;
          esac
          shift
      done

      return 0

    else return 1
    fi
}

add_luns_seq ()
{
    COUNTER=$1

    while [ $COUNTER -lt $NOLUNS ]
    do
       $ROOT_DIR/lun_hotplug.sh -v $VM_NO -l $COUNTER -s $LUN_SIZE
       if test -f lun_hotplug_err;
       then
            rm lun_hotplug_err
            exit
       fi
       COUNTER=`expr $COUNTER + 1`
       sleep $TIME_INTERVAL
    done

    return 0
}

add_luns_rnd ()
{
    COUNTER=$1

    RANGE=`expr $NOLUNS - $ADD_LUN`
    for i in `seq 1 $RANGE`
    do
       RND_LUN=$(shuf -i $ADD_LUN-$RANGE -n '1')
       echo $RND_LUN
       $ROOT_DIR/lun_hotplug.sh -v $VM_NO -l $RND_LUN -s $LUN_SIZE
       sleep $TIME_INTERVAL
    done

    return 0
}

del_luns_seq ()
{
    COUNTER=$1

    while [ $COUNTER -gt $ADD_LUN ]
    do
       COUNTER=`expr $COUNTER - 1`
       $ROOT_DIR/lun_hotplug.sh -v $VM_NO -d $COUNTER
    done

    return 0
}

del_luns_rnd ()
{
    RANGE=`expr $NOLUNS - $ADD_LUN`
    for i in `seq 1 $RANGE`
    do
       RND_LUN=$(shuf -i $ADD_LUN-$RANGE -n '1')
       $ROOT_DIR/lun_hotplug.sh -v $VM_NO -d $RND_LUN
       sleep $TIME_INTERVAL
    done

    return 0
}

del_test_images ()
{
    COUNTER=$1

    while [ $COUNTER -lt $NOLUNS ]
    do
       rm $VMWHQL_DIR/../images/client${VM_NO}_test_image${COUNTER}.raw
       COUNTER=`expr $COUNTER + 1`
    done

    return 0
}

validate_and_adjust_parameters ()
{
    test x"${LUN_SIZE}" = x"" && LUN_SIZE=$DEFAULT_LUN_SIZE

    VM_LUNS=$(vm_var_val $VM_NO LUNS)
    test x"${VM_LUNS}" = x"" && VM_LUNS=1
    if [ $ADD_LUN -ne $VM_LUNS ]
    then
        echo "Lun number to be added must be sequential to the current number of LUNs"
        exit
    fi
    if [ x"${USER_NAME}" != x"" ] && [ x"${PASSWORD}" != x"" ]; then
        VM_ID="-u $USER_NAME -p $PASSWORD -s $SETUP_ID -v $VM_NO"
    elif test -f vm_credentials.txt;
    then
        VM_ID="-s $SETUP_ID -v $VM_NO"
    else
        echo "Guest Username and Password must be provided"
        return 1
    fi

    return 0

}

#SCRIPT ENTRY POINT IS HERE

ROOT_DIR=$(dirname $0)
VMWHQL_DIR=../$(dirname $0)
SCRIPT_NAME=$(basename $0)

. $VMWHQL_DIR/vmwhql_setup.cfg
. $VMWHQL_DIR/vmwhql_env.sh

DEFAULT_LUN_SIZE="30"

if test x`whoami` != xroot
then
    echo This script must be run as superuser
    exit 1
fi

process_command_line "$@"
if [ $? != "0" ]
then
    usage
    exit $?
fi

validate_and_adjust_parameters
if [ $? != "0" ]
then
    exit $?
fi

python $ROOT_DIR/vmwhql_remoting.py $VM_ID -c "$COMMAND_TO_EXECUTE" &
sleep $TIME_INTERVAL

for i in `seq 1 $ITERATIONS`
do
    add_luns_seq $ADD_LUN

    del_luns_rnd

    add_luns_rnd $ADD_LUN

done

del_luns_seq $NOLUNS
del_test_images $ADD_LUN

python $ROOT_DIR/vmwhql_remoting.py $VM_ID -c "echo stopstress > d:\tests\stopstress.txt"
