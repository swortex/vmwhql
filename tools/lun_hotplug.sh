#!/bin/sh
#
# Copyright (c) 2014, Swortex A.A. Ltd (www.swortex.com)
# Copyright (c) 2013, Daynix Computing LTD (www.daynix.com)
# All rights reserved.
#
# Maintained by vmwhql@swortex.com
#
# This code is licensed under standard 3-clause BSD license.
# See file LICENSE supplied with this package for the full license text.

VMWHQL_DIR=$(readlink -f ../$(dirname $0))
SCRIPT_NAME=$(basename $0)
usage ()
{
    echo "$SCRIPT_NAME - Dynamic LUN management"
    echo ""
    echo "Usage:"
    echo "  options:"
    echo "      -h"
    echo "      --help - show this help"
    echo "      -p <path>"
    echo "      --path <path> - set path to socket (default path is $DEFAULT_PATH_TO_SOCKET)"
    echo "      -s <lun-size>"
    echo "      --lun-size <lun-size> - set desired LUN size (default size is ${DEFAULT_LUN_SIZE}GB)"
    echo "      -v <vm-number>"
    echo "      --vm-number <vm-number> - set desired vm number"
    echo "      -l <add-lun>"
    echo "      --add-lun <add-lun> - set number of LUN to be added"
    echo "      -d <delete-lun>"
    echo "      --delete-lun <delete-lun> - set number of lun to be deleted"

   return 0
}
process_command_line ()
{
    if [ "$1" != "" ]
    then
      while [ "$1" != "" ]
      do
          case $1 in
              -h|--help)
                  return 1
                  ;;
              -p|--path)
                  shift
                  PATH_TO_SOCKET=$1
                  ;;
              -s|--lun-size)
                  shift
                  LUN_SIZE=$1
                  ;;
              -v|--vm-number)
                  shift
                  VM_NO=$1
                  ;;
              -l|--add-lun)
                  shift
                  ADD_LUN_NO=$1
                  ;;
              -d|--delete-lun)
                  shift
                  DEL_LUN_NO=$1
                  ;;
          esac
          shift
      done

      return 0

    else return 1
    fi
}
validate_and_adjust_parameters ()
{
    VM_LUNS=$(vm_var_val $VM_NO LUNS)
    test x"${VM_LUNS}" = x"" && VM_LUNS=1 # 1 is the default number of LUNs value
    test -f lun_hotplug_err && rm lun_hotplug_err
    if [ x"${ADD_LUN_NO}" != x"" ] && [ $ADD_LUN_NO -lt $VM_LUNS ]
    then
        echo "Lun number to be added must be higher than current lun number"
        touch lun_hotplug_err
        return 1
    fi
    if [ x"${ADD_LUN_NO}" = x"" ] && [ x"${DEL_LUN_NO}" = x"" ]
    then
        echo "Please specify the desired operation (add or delete LUN)"
        return 1
    fi
    if [ x"${ADD_LUN_NO}" != x"" ] && [ x"${DEL_LUN_NO}" != x"" ]
    then
        echo "Add and delete lun are mutually exclusive parameters"
        return 1
    fi
    if [ ! -S $PATH_TO_SOCKET/qemu_monitor_${UNIQUE_ID}_client${VM_NO}_socket ]
    then
        echo "Socket doesn't exist"
        touch lun_hotplug_err
        return 1
    fi
    return 0
}
add_lun ()
{
    VM_NO=$1
    ADD_LUN_NO=$2
    test -f $VMWHQL_DIR/../images/client${VM_NO}_test_image${ADD_LUN_NO}.raw || \
    { echo Creating test image client${VM_NO}_test_image${ADD_LUN_NO}.raw...; qemu-img create -f raw $VMWHQL_DIR/../images/client${VM_NO}_test_image${ADD_LUN_NO}.raw ${LUN_SIZE}G; }
    echo drive_add 0 file=./images/client${VM_NO}_test_image${ADD_LUN_NO}.raw,if=none,id=scsi${ADD_LUN_NO} | socat - UNIX-CONNECT:$PATH_TO_SOCKET/qemu_monitor_${UNIQUE_ID}_client${VM_NO}_socket
    echo device_add scsi-hd,drive=scsi${ADD_LUN_NO},scsi-id=0,bus=scsi.0,lun=${ADD_LUN_NO},id=scsi${ADD_LUN_NO} | socat - UNIX-CONNECT:$PATH_TO_SOCKET/qemu_monitor_${UNIQUE_ID}_client${VM_NO}_socket

    return 0
}
del_lun ()
{
    VM_NO=$1
    DEL_LUN_NO=$2
    echo device_del scsi${DEL_LUN_NO} | socat - UNIX-CONNECT:$PATH_TO_SOCKET/qemu_monitor_${UNIQUE_ID}_client${VM_NO}_socket

    return 0
}


# SCRIPT ENTRY POINT IS HERE

DEFAULT_LUN_SIZE="30"
DEFAULT_PATH_TO_SOCKET=$VMWHQL_DIR

if test x`whoami` != xroot
then
    echo This script must be run as superuser
    exit 1
fi

process_command_line "$@"
if [ $? != "0" ]
then
    usage
    exit $?
fi

test x"${PATH_TO_SOCKET}" = x"" && PATH_TO_SOCKET=$DEFAULT_PATH_TO_SOCKET
test x"${LUN_SIZE}" = x"" && LUN_SIZE=$DEFAULT_LUN_SIZE

. $VMWHQL_DIR/vmwhql_setup.cfg
. $VMWHQL_DIR/vmwhql_env.sh

validate_and_adjust_parameters
if [ $? != "0" ]
then
    exit $?
fi

if test x"${DEL_LUN_NO}" != x""
then
    del_lun ${VM_NO} ${DEL_LUN_NO}
fi

if test x"${ADD_LUN_NO}" != x""
then
    add_lun ${VM_NO} ${ADD_LUN_NO}
fi
