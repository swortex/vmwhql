if test x"$UNIQUE_ID" = x""; then
    echo "ERROR: vmwhql_setup.cfg must be included"
    exit 1
fi

#BRIDGES
WORLD_BR_NAME=whqlbr0
CTRL_BR_NAME=ctrltestbr_${UNIQUE_ID}
TEST_BR_NAME=hcktestbr_${UNIQUE_ID}
MAX_VMs_PER_SETUP=5 # If MAX_VMs_PER_SETUP changes, appropriate change must be made in /tools/vmwhql_remoting.py as well
VNC_PORT_BASE=5900
REMOTE_PORT_BASE=50000 # If REMOTE_PORT_BASE changes, appropriate change must be made in /tools/vmwhql_remoting.py as well

remove_bridges() {
    case $TEST_NET_TYPE in
        bridge)
            ifconfig ${TEST_BR_NAME} down
            brctl delbr ${TEST_BR_NAME}
            ;;
        OVS)
            ovs-vsctl del-br ${TEST_BR_NAME}
            ;;
    esac

    ifconfig ${CTRL_BR_NAME} down
    brctl delbr ${CTRL_BR_NAME}
}

create_bridges() {
    case $TEST_NET_TYPE in
        bridge)
            brctl addbr ${TEST_BR_NAME} 2>&1 | grep -v "already exists"
            ifconfig ${TEST_BR_NAME} up
            ;;
        OVS)
            ovs-vsctl add-br ${TEST_BR_NAME}
            ;;
    esac

    brctl addbr ${CTRL_BR_NAME} 2>&1 | grep -v "already exists"
    ifconfig ${CTRL_BR_NAME} up
}

modify_iface_offload_settings() {
    IFNAME=$1
    if test x"$DISABLE_TX_OFFLOAD" = x"on"; then
        ethtool -K ${IFNAME} tx off
    fi
}

enslave_iface() {
    BRNAME=$1
    IFNAME=$2

    ifconfig ${IFNAME} promisc 0.0.0.0 &&
    brctl addif ${BRNAME} ${IFNAME} &&
    modify_iface_offload_settings ${IFNAME}
}

enslave_test_iface() {
    BRNAME=$1
    IFNAME=$2

    ifconfig ${IFNAME} promisc 0.0.0.0 &&

    case $TEST_NET_TYPE in
        bridge)
            brctl addif ${BRNAME} ${IFNAME} ||
            echo ERROR: Failed to enslave ${IFNAME} to ${BRNAME} bridge
            ;;
        OVS)
            { ovs-vsctl add-port ${BRNAME} ${IFNAME} &&
            ovs-vsctl set port ${IFNAME} other-config:priority-tags=true; } ||
            echo ERROR: Failed to enslave ${IFNAME} to ${BRNAME} ovs-bridge
            ;;
    esac

    if test x"$DISABLE_TX_OFFLOAD" = x"on"; then
        ethtool -K ${IFNAME} tx off
    fi
    ifconfig ${IFNAME} txqueuelen 1000
}

vm_video_port_no() {
    VM_NO=$1
    PORT_BASE=`expr ${UNIQUE_ID} '*' ${MAX_VMs_PER_SETUP}`
    echo `expr ${PORT_BASE} + $VM_NO - 1`
}

vm_host_winrm_port_no() # If vm_host_winrm_port_no formula changes, appropriate change must be made in /tools/vmwhql_remoting.py as well
{
    VM_NO=$1
    PORT_BASE=`expr ${UNIQUE_ID} '*' ${MAX_VMs_PER_SETUP}`
    echo `expr ${REMOTE_PORT_BASE} + ${PORT_BASE} + $VM_NO - 1`
}

vm_video_port_for_print() {
    VM_NO=$1
    PORT=$(vm_video_port_no $VM_NO)
    PORT_BASE=0
    if test x"$VIDEO_TYPE" = x"VNC"; then
        PORT_BASE=$VNC_PORT_BASE
    fi
    echo `expr ${PORT_BASE} + $PORT`
}

vm_var_val()
{
    VM_NO=$1
    VM_VAR=$2

    VAR_NAME=VM${VM_NO}_${VM_VAR}
    eval echo \$${VAR_NAME}
}

dump_vm_config()
{
    VM_NO=$1
    echo "  VM No. ${VM_NO} configuration"
    echo "    VM Display port............ $(vm_video_port_for_print $VM_NO)"
    if test x"${VM_REMOTING}" = xon; then
            echo "    VM Host WinRM  port........ $(vm_host_winrm_port_no $VM_NO)"
    fi
    echo "    VM Image................... $(vm_var_val $VM_NO IMAGE)"
    echo "    VM Role.................... $(vm_var_val $VM_NO ROLE)"
    echo "    VM CPUs.................... $(vm_var_val $VM_NO CPUS)"
    echo "    VM Memory.................. $(vm_var_val $VM_NO MEMORY)"
        VM_ROLE=$(vm_var_val $VM_NO ROLE)
        if test x"$HCK_TEST_DEV_TYPE" = x"network" && test x"$VM_ROLE" = x"hck_client"; then
                VM_NOQ=$(vm_var_val $VM_NO NOQ)
                if test x"$VM_NOQ" = x""; then
                        VM_NOQ="1"
                fi
                echo "    VM Network queues.......... ${VM_NOQ}"
        fi
        if test x"$HCK_TEST_DEV_TYPE" = x"storage" && test x"$VM_ROLE" = x"hck_client"; then
                echo "    VM Test image.............. client${VM_NO}_test_image.raw"
        fi
        if test x"$HCK_TEST_DEV_TYPE" = x"scsi" && test x"$VM_ROLE" = x"hck_client"; then
                VM_NOQ=$(vm_var_val $VM_NO NOQ)
                if test x"$VM_NOQ" = x""; then
                        VM_NOQ="1"
                fi
                echo "    VM SCSI Request queues..... ${VM_NOQ}"
                VM_LUNS=$(vm_var_val $VM_NO LUNS)
                if test x"$VM_LUNS" = x""; then
                        VM_LUNS="1"
                fi
                echo "    VM SCSI Logical units...... ${VM_LUNS}"
                LUN_SERIAL_NUM=0
                while [ $LUN_SERIAL_NUM -lt $VM_LUNS ]
                do
                   echo "    VM Test image $LUN_SERIAL_NUM............ "client${VM_NO}_test_image"$LUN_SERIAL_NUM.raw"
                   LUN_SERIAL_NUM=`expr $LUN_SERIAL_NUM + 1`
                done
                VM_CONSOLE_TO_SOCKET=$(vm_var_val $VM_NO CONSOLE_TO_SOCKET)
                if test x"${VM_CONSOLE_TO_SOCKET}" = xon; then
                        echo "    VM Qemu console socket..... "`dirname $0`/qemu_monitor_"${UNIQUE_ID}_client${VM_NO}_socket"
                fi
        fi
    echo "    VM Extra................... $(vm_var_val $VM_NO EXTRA)"
    VM_CPU_MODEL=$(vm_var_val $VM_NO CPU_MODEL)
    if test x"$VM_CPU_MODEL" != x""; then
        echo "    VM CPU Model............... ${VM_CPU_MODEL}"
    fi
}

dump_config()
{
    echo "Setup configuration:"
    echo "  Setup ID................... ${UNIQUE_ID}"
    echo "  Test suite type............ ${VMWHQL_SETUP_TYPE}"
    if test x"$VMWHQL_SETUP_TYPE" = x"hck"; then
        echo "  HCK test type.............. ${HCK_TEST_DEV_TYPE}"
    fi
    if test x"${VM_REMOTING}" = xon; then
        echo "  VM Shared folder........... /tmp/vmwhql_smb/${UNIQUE_ID}"
    fi
    echo "  Graphics................... ${VIDEO_TYPE}"
    echo "  Test network backend....... ${TEST_NET_TYPE}"
    echo "  QEMU binary................ ${QEMU_BIN}"
    echo "  World network device....... ${WORLD_NET_DEVICE}"
    echo "  Control network device..... ${CTRL_NET_DEVICE}"
    if test x"$VMWHQL_SETUP_TYPE" = x"svvp"; then
        echo "  SVVP Client network device. ${SVVP_CL_NET_DEVICE}"
    fi
    echo "  VHOST...................... ${VHOST_STATE}"
    echo "  Snapshot mode.............. ${SNAPSHOT}"
    echo "  Unsafe Cache mode ......... ${UNSAFE_CACHE}"

    for i in `seq 1 $NOF_VMS`
    do
        dump_vm_config $i
    done
}

LOOPRUN_FILE=`dirname $0`/.hck_stop_looped_vms.flag

loop_run_vm() {
    while true; do
        $*
        test -f $LOOPRUN_FILE && return 0
        sleep 2
    done
}

loop_run_stop() {
    touch $LOOPRUN_FILE
}

loop_run_reset() {
    rm -f $LOOPRUN_FILE
}

is_integer()
{
    case $string in
        ''|*[!0-9]*)
            return 0
            ;;
        *)
            return 1
            ;;
    esac
}

check_global_cfg() {
    test $VMWHQL_SETUP_TYPE != "svvp" -a $VMWHQL_SETUP_TYPE != "hck" && { echo "ERROR: Unknown VMWHQL setup type: $VMWHQL_SETUP_TYPE" && return 1; }
    test -z $HCK_ROOT && { echo "ERROR: HCK_ROOT must be specified" && return 1; }
    test ! -d $HCK_ROOT && { echo "ERROR: Specified HCK_ROOT does not exist" && return 1; }
    test -z $QEMU_BIN && { echo "ERROR: QEMU_BIN must be specified" && return 1; }
    if ! $QEMU_BIN -version ; then
                echo "ERROR: QEMU_BIN=$QEMU_BIN cannot be executed"
                return 1
        fi
    test -z ${WORLD_NET_DEVICE} && { echo "ERROR: WROLD_NET_DEVICE not specified" && return 1; }
    test -z ${CTRL_NET_DEVICE} && { echo "ERROR: CTRL_NET_DEVICE not specified" && return 1; }
    if ! is_integer $UNIQUE_ID ; then
        echo "ERROR: UNIQUE_ID must be an integer"
        return 1
    fi
    test ${#UNIQUE_ID} -ne 2 && { echo "ERROR: UNIQUE_ID must be of 2 digits" && return 1; }
    return 0
}

